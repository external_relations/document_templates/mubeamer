#!/bin/bash
# Prepares and submits the Overleaf archives for every workplace.
#
# Usage: ./install-overleaf.sh [OPTION...]
#
# Options:
#
#   --only-generate : only produces the dist/ and overleaf/ directories
#   --only-upload   : only uploads the overleaf/ directories to Overleaf over Git
#   --only-publish  : only publishes the Overleaf documents to Overleaf Gallery
set -e
shopt -s extglob
shopt -s nullglob

WORKPLACES=(arch c4e ceitec cerit ctt czs econ fi fsps fss iba ics is kariera \
  lang law med mu muzeu ped pharm phil press ptceitec rect rect-office sci skm teiresias ucb \
  uct)

if [[ $# != 0 && $1 != --only-publish && $1 != --only-upload && $1 != --only-generate ]]; then
  printf 'Unexpected "%s" parameter.\n' "$1" 2>&1
  exit 1
fi

if [[ $1 != --only-publish && $1 != --only-upload ]]; then

  # Clean up
  rm -rf /tmp/overleaf overleaf dist

  # Prepare the files
  xetex mubeamer.ins
  latexmk -pdf mubeamer.dtx
  latexmk -c mubeamer.dtx
  rm *.{bbl,bib,glo,gls,hd,run.xml}
  pushd example
  xetex example.ins
  popd
  mkdir /tmp/overleaf

  for WORKPLACE in ${WORKPLACES[@]}; do
    TMPDIR=/tmp/overleaf/$WORKPLACE
    mkdir -p $TMPDIR/mubeamer
    # Copy top-level example files
    cp -v example/{$WORKPLACE{,-aspect-16_{9,10},-czech,-slovak,-fonts-lmodern,-gray,-xetex-luatex}.tex,example.bib} \
      beamerthemeMU.sty `kpsewhich cow-black.mps` $TMPDIR
    # Copy remaining files
    (
      shopt -s nullglob
      tar cv {logo,label}/mubeamer-{mu,$WORKPLACE}-*.pdf mubeamer.{dtx,pdf,ins} \
        LICENSE.tex README.md | tar xC $TMPDIR/mubeamer
    )
    if test -e patch/mubeamer-$WORKPLACE.sty; then
      tar cv patch/mubeamer-$WORKPLACE.sty | tar xC $TMPDIR/mubeamer
    fi
    pushd $TMPDIR
    # Typeset top-level example files
    for DOCUMENT in $WORKPLACE{,-!(xetex-luatex)}.tex; do
       latexmk -pdf $DOCUMENT &
    done
    latexmk -lualatex $WORKPLACE-xetex-luatex.tex &
    wait
    latexmk -c $WORKPLACE{,-*}.tex
    rm -rf par-*/
    rm *.{nav,snm,run.xml,bbl}
    # Prepare a zip archive
    zip -r ../$WORKPLACE *
    popd
  done

  mv -v /tmp/overleaf dist
  cp -v -r --reflink=always dist overleaf
  rm overleaf/{*.zip,*/{cow-black.mps,mubeamer/*.*}}

  for WORKPLACE in ${WORKPLACES[@]}; do
    rm overleaf/$WORKPLACE/$WORKPLACE*.pdf
  done

fi

if [[ $1 != --only-publish && $1 != --only-generate ]]; then

  # Upload to Git.
  parallel --halt=2 --bar --jobs 1 -- ./install-overleaf-git-upload.sh ::: ${WORKPLACES[@]}

fi

if [[ $1 != --only-upload && $1 != --only-publish && $1 != --only-generate ]]; then

  # Sleep for a while to make sure that Overleaf is aware that a new version
  # has been uploaded.
  sleep 5s

fi

if [[ $1 != --only-upload && $1 != --only-generate ]]; then

  # Publish to Overleaf gallery.
  parallel --halt=2 --bar --jobs 1 -- './overleaf-upload.sh {}' ::: \
    `for WORKPLACE in ${WORKPLACES[@]}; do echo overleaf-meta/$WORKPLACE/overleaf-upload.def; done`

fi

if [[ $1 != --only-upload && $1 != --only-publish && $1 != --only-generate ]]; then

  rm -rf overleaf

fi
