#!/bin/sh
# Submits the current version of the package to the webserver via aisa.
set -e
cd dist
tar cz *.zip | ssh aisa 'sg wwwlemma -c "set -e; tar xzvC /www/lemma/projekty/mubeamer/; make -C /www/lemma/projekty/mubeamer/ index.html"'
